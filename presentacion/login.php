<body style=" background-color: #17a2b8;">    

<div class="container" style="padding-top:50">    
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title">Sign In</div>  
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a>Sutec Gestion Obra Civil</a></div>                     
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="login-form" class="form" role="form" action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>" method="post">
                                    
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                        <input type="text" class="form-control" name="correo" id="correo" value="" placeholder="username or email" require>                                        
                                    </div>
                                
                            <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" class="form-control" name="clave" id="clave" placeholder="password" require>
                                    </div>                                                                                            

                                <div style="margin-top:10px" class="form-group">
                                    <!-- Button -->

                                    <div class="col-sm-4 controls">                                      
                                    <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
                                    </div>
                                    <div class="col-sm-4 controls"> </div>
                                    <div class="col-sm-4 controls">
                                    <img src="img/logo.png" class="img-fluid" alt="Responsive image"> 
                                    </div>
                                </div> 
                            </form>     
                        </div>   
                        
                    </div>  
                    <?php
							if (isset($_GET["error"]) && $_GET["error"] == 1) {
								echo "<div class=\"alert alert-danger\" role=\"alert\">Contraseña o Correo incorrecto</div>";
							} else if (isset($_GET["error"]) && $_GET["error"] == 2) {
								echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta no ha sido activada</div>";
							} else if (isset($_GET["error"]) && $_GET["error"] == 3) {
								echo "<div class=\"alert alert-danger\" role=\"alert\">Su cuenta ha sido inhabilitada</div>";
							}
							?>
        </div>
        
</div>

</body>
