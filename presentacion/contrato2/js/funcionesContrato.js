function cambiarCantida(cantidad){		
	
	var url = "indexAjax.php?pid="+btoa('presentacion/contrato2/componentes/tabla.php')+"&cantidad="+cantidad+"&rol="+$('#rol').val();
	$("#tabla").load(url);
	
} 

function pagina(pagina,cantidad){		
	
	var url = "indexAjax.php?pid="+btoa('presentacion/contrato2/componentes/tabla.php')+"&cantidad="+cantidad+"&pagina="+pagina+"&rol="+$('#rol').val();
	$("#tabla").load(url);
	
} 
 
function agregardatos(nombre, tipoDocumento, numDocumento, cargo, telefono1, telefono2, correo, salario, tipoContrato, estadoContrato, proyecto, fechaInicioProceso, fechaEnvioConsorcio, fechaPreAprobado, fechaEnvioInterventoria, fechaRespuestaInterventoria, fechaIngresoALaborar, fechaTerminacionContrato, tipoTerminacionContrato, fechaEntregaCarnet, fechaEntregaDotacion, entidadBancaria, certificacionBancaria, sexo, edad, fechadeExpedicion, lugarExpedicion, fechaNacimiento, lugarNacimiento, nacionalidad, fechaAfilacionARL, ARL, fechaAfilacionEPS, EPS, fechaAfilacionCCF, AFP, residencia, barrioResidencia, direccionResidencia, tipoPerfil, tipoTrabajo, poblacionVulnerable ){

	cadena="nombre="+nombre+
	"&tipoDocumento="+tipoDocumento+
	"&numDocumento="+numDocumento+
	"&cargo="+cargo+
	"&telefono1="+telefono1+
	"&telefono2="+telefono2+
	"&correo="+correo+
	"&salario="+salario+
	"&tipoContrato="+tipoContrato+
	"&estadoContrato="+estadoContrato+
	"&proyecto="+proyecto+
	"&fechaInicioProceso="+fechaInicioProceso+
	"&fechaEnvioConsorcio="+fechaEnvioConsorcio+
	"&fechaPreAprobado="+fechaPreAprobado+
	"&fechaEnvioInterventoria="+fechaEnvioInterventoria+
	"&fechaRespuestaInterventoria="+fechaRespuestaInterventoria+
	"&fechaIngresoALaborar="+fechaIngresoALaborar+
	"&fechaTerminacionContrato="+fechaTerminacionContrato +
	"&tipoTerminacionContrato="+tipoTerminacionContrato +
	"&fechaEntregaCarnet="+fechaEntregaCarnet +
	"&fechaEntregaDotacion="+fechaEntregaDotacion +
	"&entidadBancaria="+entidadBancaria+
	"&certificacionBancaria="+certificacionBancaria +
	"&sexo="+sexo+
	"&edad="+edad+
	"&fechadeExpedicion="+fechadeExpedicion+
	"&lugarExpedicion="+lugarExpedicion+
	"&fechaNacimiento="+fechaNacimiento+
	"&lugarNacimiento="+lugarNacimiento+            
	"&nacionalidad="+nacionalidad+        
	"&fechaAfilacionARL="+fechaAfilacionARL+
	"&ARL="+ARL+
	"&fechaAfilacionEPS="+fechaAfilacionEPS+
	"&EPS="+EPS+
	"&fechaAfilacionCCF="+fechaAfilacionCCF+        
	"&AFP="+AFP+
	"&residencia="+residencia+
	"&barrioResidencia="+barrioResidencia+
	"&direccionResidencia="+direccionResidencia+
	"&tipoPerfil="+tipoPerfil+
	"&tipoTrabajo="+tipoTrabajo+
	"&poblacionVulnerable="+poblacionVulnerable;
	
	$.ajax({
		type:"POST",
		url:"indexAjax.php?pid="+btoa('presentacion/contrato2/php/agregarDatos.php'),
		data:cadena,
		success:function(r){
			if(r==1){
				
				var url = "indexAjax.php?pid="+btoa('presentacion/contrato2/componentes/tabla.php')+"&rol="+$('#rol').val();	
				$("#tabla").load(url);

				alertify.success("agregado con exito :)");
			}else{
				alertify.error("Fallo el servidor :(");
			}
		}
	});
 
} 

function actualizaDatosEditarColumn(editableObj,nuevo,column,item){	
	
	cadena="nuevo=" + nuevo + 
			"&column=" + column +
			"&item=" + item;			;
	
	$.ajax({
		type:"POST",
		url:"indexAjax.php?pid="+btoa('presentacion/contrato2/php/actualizaDatos.php'),
		data:cadena,
		success:function(r){
			if(r==1){
				
				$(editableObj).css("background","#FDFDFD");								
				
				alertify.success("Actulizado con exito :)");
			}else{
				alertify.error("Fallo el servidor :(");
			}
		}
	});

} 

function agregaform(datos){

	d=datos.split('||');

	$('#itemu').val(d[0]);
	$('#nombreu').val(d[1]);
	$('#tipoDocumentou').val(d[2]);
	$('#numDocumentou').val(d[3]);
	$('#cargou').val(d[4]);
	$('#telefono1u').val(d[5]);
	$('#telefono2u').val(d[6]);
	$('#correou').val(d[7]);
	$('#salariou').val(d[8]);
	$('#tipoContratou').val(d[9]);
	$('#estadoContratou').val(d[10]);
	$('#proyectou').val(d[11]);
	$('#fechaInicioProcesou').val(d[12]);
	$('#fechaEnvioConsorciou').val(d[13]);
	$('#fechaPreAprobadou').val(d[14]);
	$('#fechaEnvioInterventoriau').val(d[15]);
	$('#fechaRespuestaInterventoriau').val(d[16]);
	$('#fechaIngresoALaboraru').val(d[17]);
	$('#fechaTerminacionContratou').val(d[18]);
	$('#tipoTerminacionContratou').val(d[19]);
	$('#fechaEntregaCarnetu').val(d[20]);
	$('#fechaEntregaDotacionu').val(d[21]);
	$('#entidadBancariau').val(d[22]);
	$('#certificacionBancariau').val(d[23]);
	$('#sexou').val(d[24]);
	$('#edadu').val(d[25]);
	$('#fechadeExpedicionu').val(d[26]);
	$('#lugarExpedicionu').val(d[27]);
	$('#fechaNacimientou').val(d[28]);
	$('#lugarNacimientou').val(d[29]);
	$('#nacionalidadu').val(d[30]);
	$('#fechaAfilacionARLu').val(d[31]);
	$('#ARLu').val(d[32]);
	$('#fechaAfilacionEPSu').val(d[33]);
	$('#EPSu').val(d[34]);
	$('#fechaAfilacionCCFu').val(d[35]);
	$('#AFPu').val(d[36]);
	$('#residenciau').val(d[37]);
	$('#barrioResidenciau').val(d[38]);
	$('#direccionResidenciau').val(d[39]);
	$('#tipoPerfilu').val(d[40]);
	$('#tipoTrabajou').val(d[41]);
	$('#poblacionVulnerableu').val(d[42]);

	
}

function agregaformV(datos){

	d=datos.split('||');

	$('#itemv').val(d[0]);
	$('#nombrev').val(d[1]);
	$('#tipoDocumentov').val(d[2]);
	$('#numDocumentov').val(d[3]);
	$('#cargov').val(d[4]);
	$('#telefono1v').val(d[5]);
	$('#telefono2v').val(d[6]);
	$('#correov').val(d[7]);
	$('#salariov').val(d[8]);
	$('#tipoContratov').val(d[9]);
	$('#estadoContratov').val(d[10]);
	$('#proyectov').val(d[11]);
	$('#fechaInicioProcesov').val(d[12]);
	$('#fechaEnvioConsorciov').val(d[13]);
	$('#fechaPreAprobadov').val(d[14]);
	$('#fechaEnvioInterventoriav').val(d[15]);
	$('#fechaRespuestaInterventoriav').val(d[16]);
	$('#fechaIngresoALaborarv').val(d[17]);
	$('#fechaTerminacionContratov').val(d[18]);
	$('#tipoTerminacionContratov').val(d[19]);
	$('#fechaEntregaCarnetv').val(d[20]);
	$('#fechaEntregaDotacionv').val(d[21]);
	$('#entidadBancariav').val(d[22]);
	$('#certificacionBancariav').val(d[23]);
	$('#sexov').val(d[24]);
	$('#edadv').val(d[25]);
	$('#fechadeExpedicionv').val(d[26]);
	$('#lugarExpedicionv').val(d[27]);
	$('#fechaNacimientov').val(d[28]);
	$('#lugarNacimientov').val(d[29]);
	$('#nacionalidadv').val(d[30]);
	$('#fechaAfilacionARLv').val(d[31]);
	$('#ARLv').val(d[32]);
	$('#fechaAfilacionEPSv').val(d[33]);
	$('#EPSv').val(d[34]);
	$('#fechaAfilacionCCFv').val(d[35]);
	$('#AFPv').val(d[36]);
	$('#residenciav').val(d[37]);
	$('#barrioResidenciav').val(d[38]);
	$('#direccionResidenciav').val(d[39]);
	$('#tipoPerfilv').val(d[40]);
	$('#tipoTrabajov').val(d[41]);
	$('#poblacionVulnerablev').val(d[42]);

	
}


function actualizaDatos(){


	id=$('#idpersona').val();
	nombre=$('#nombreu').val();
	apellido=$('#apellidou').val();
	email=$('#emailu').val();
	clave=$('#claveu').val();
	rol=$('#rolu').val();

	cadena= "id=" + id +
			"&nombre=" + nombre + 
			"&apellido=" + apellido +
			"&email=" + email +
			"&clave=" + clave +
			"&rol=" + rol;

	$.ajax({
		type:"POST",
		url:"presentacion/contrato2/php/actualizaDatos.php",
		data:cadena,
		success:function(r){
			
			if(r==1){
				//$('#tabla').load('presentacion/contrato2/componentes/tabla.php');
				var url = "indexAjax.php?pid="+btoa('presentacion/contrato2/componentes/tabla.php')+"&rol="+$('#rol').val();	
				$("#tabla").load(url);
				alertify.success("Actualizado con exito :)");
			}else{
				alertify.error("Fallo el servidor :( act");
			}
		}
	});

}

function preguntarSiNo(id){
	alertify.confirm('Eliminar Datos', '¿Esta seguro de eliminar este registro?', 
					function(){ eliminarDatos(id) }
                , function(){ alertify.error('Se cancelo')});
}

function eliminarDatos(id){

	cadena="id=" + id;

		$.ajax({
			type:"POST",
			url:"indexAjax.php?pid="+btoa('presentacion/contrato2/php/eliminarDatos.php'),
			data:cadena,
			success:function(r){
				if(r==1){
					//$('#tabla').load('presentacion/contrato2r/componentes/tabla.php');
					var url = "indexAjax.php?pid="+btoa('presentacion/contrato2/componentes/tabla.php')+"&rol="+$('#rol').val();	
					$("#tabla").load(url);
					alertify.success("Eliminado con exito!");
				}else{
					alertify.error("Fallo el servidor :( del");
				}
			}
		});
}