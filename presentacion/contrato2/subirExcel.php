<?php
include('dbconect.php');
require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

    require_once "logica/Contrato.php";
    date_default_timezone_set ("America/Bogota");	
	
	

if (isset($_POST["import"]))
{
    
$allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
  
  if(in_array($_FILES["file"]["type"],$allowedFileType)){

        $targetPath = 'subidas/'.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
        
        $Reader = new SpreadsheetReader($targetPath);
        
        $sheetCount = count($Reader->sheets());
        
        for($i=0;$i<$sheetCount;$i++)
        {            
            $Reader->ChangeSheet($i);
                        
            $primera = 1;
            $ii=0;   
            foreach ($Reader as $Row)
            { $ii++;
          
                                // Evitamos la primer linea
                if($primera<=2){
                    $primera++;
                    continue;
                    }                
             
//---------------------------------------------------------------------------------------------------

	$item ="";        
	$item = time()+$ii;

    $nombre ="";        
	if(isset($Row[1])) { 
		$nombre = mysqli_real_escape_string($con,$Row[1]);    }    

        $tipoDocumento ="";        
	if(isset($Row[2])) { 
		$tipoDocumento = mysqli_real_escape_string($con,$Row[2]);    }

        $numDocumento ="";        
	if(isset($Row[3])) { 
		$numDocumento = mysqli_real_escape_string($con,$Row[3]);    }

    $cargo ="";        
	if(isset($Row[4])) { 
        $cargo = mysqli_real_escape_string($con,$Row[4]);    }     
        
    $telefono1 ="";        
	if(isset($Row[5])) { 
		$telefono1 = mysqli_real_escape_string($con,$Row[5]);    } 

    $telefono2 ="";        
	if(isset($Row[6])) { 
        $telefono2 = mysqli_real_escape_string($con,$Row[6]);    } 
    
    $correo ="";        
	if(isset($Row[7])) { 
        $correo = mysqli_real_escape_string($con,$Row[7]);    } 
    
    $salario ="";        
	if(isset($Row[8])) { 
        $salario = mysqli_real_escape_string($con,$Row[8]);    } 
    
    $tipoContrato ="";        
	if(isset($Row[9])) { 
        $tipoContrato = mysqli_real_escape_string($con,$Row[9]);    }        

    $estadoContrato ="";        
	if(isset($Row[10])) { 
        $estadoContrato = mysqli_real_escape_string($con,$Row[10]);    } 
    
    $proyecto ="";        
	if(isset($Row[11])) { 
		$proyecto = mysqli_real_escape_string($con,$Row[11]);    } 

    $fechaInicioProceso ="";        
	if(isset($Row[12])) { 
        $fechaInicioProceso = mysqli_real_escape_string($con,$Row[12]);    } 
    
    $fechaEnvioConsorcio ="";        
	if(isset($Row[13])) { 
        $fechaEnvioConsorcio = mysqli_real_escape_string($con,$Row[13]);    }
    
    $fechaPreAprobado ="";        
	if(isset($Row[14])) { 
        $fechaPreAprobado = mysqli_real_escape_string($con,$Row[14]);    }

    $fechaEnvioInterventoria ="";        
	if(isset($Row[15])) { 
        $fechaEnvioInterventoria = mysqli_real_escape_string($con,$Row[15]);    }
    
    $fechaRespuestaInterventoria ="";        
	if(isset($Row[16])) { 
        $fechaRespuestaInterventoria = mysqli_real_escape_string($con,$Row[16]);    }

    $fechaIngresoALaborar ="";        
	if(isset($Row[17])) { 
		$fechaIngresoALaborar = mysqli_real_escape_string($con,$Row[17]);    }
	
	$fechaTerminacionContrato ="";        
	if(isset($Row[18])) { 
		$fechaTerminacionContrato  = mysqli_real_escape_string($con,$Row[18]);    } 

    $tipoTerminacionContrato ="";        
	if(isset($Row[19])) { 
		$tipoTerminacionContrato  = mysqli_real_escape_string($con,$Row[19]);    } 

    $fechaEntregaCarnet ="";        
	if(isset($Row[20])) { 
		$fechaEntregaCarnet = mysqli_real_escape_string($con,$Row[20]);    } 

    $fechaEntregaDotacion ="";        
	if(isset($Row[21])) { 
		$fechaEntregaDotacion  = mysqli_real_escape_string($con,$Row[21]);    } 

    $entidadBancaria ="";        
	if(isset($Row[22])) { 
		$entidadBancaria = mysqli_real_escape_string($con,$Row[22]);    } 

    $certificacionBancaria ="";        
	if(isset($Row[23])) { 
		$certificacionBancaria  = mysqli_real_escape_string($con,$Row[23]);    } 

	$sexo ="";        
	if(isset($Row[24])) { 
		$sexo = mysqli_real_escape_string($con,$Row[24]);    } 

	$edad ="";        
	if(isset($Row[25])) { 
		$edad = mysqli_real_escape_string($con,$Row[25]);    }      

    $fechadeExpedicion ="";        
	if(isset($Row[26])) { 
		$fechadeExpedicion = mysqli_real_escape_string($con,$Row[26]);    } 

    $lugarExpedicion ="";        
	if(isset($Row[27])) { 
		$lugarExpedicion = mysqli_real_escape_string($con,$Row[27]);    } 

    $fechaNacimiento ="";        
	if(isset($Row[28])) { 
		$fechaNacimiento = mysqli_real_escape_string($con,$Row[28]);    } 

    $lugarNacimiento ="";        
	if(isset($Row[29])) { 
		$lugarNacimiento = mysqli_real_escape_string($con,$Row[29]);    } 
     
    $nacionalidad ="";        
	if(isset($Row[30])) { 
		$nacionalidad = mysqli_real_escape_string($con,$Row[30]);    }         

    $fechaAfilacionARL ="";        
	if(isset($Row[31])) { 
		$fechaAfilacionARL = mysqli_real_escape_string($con,$Row[31]);    } 

    $ARL ="";        
	if(isset($Row[32])) { 
		$ARL = mysqli_real_escape_string($con,$Row[32]);    } 

    $fechaAfilacionEPS ="";        
	if(isset($Row[33])) { 
		$fechaAfilacionEPS = mysqli_real_escape_string($con,$Row[33]);    } 

    $EPS ="";        
	if(isset($Row[34])) { 
		$EPS = mysqli_real_escape_string($con,$Row[34]);    } 

    $fechaAfilacionCCF ="";        
	if(isset($Row[34])) { 
		$fechaAfilacionCCF = mysqli_real_escape_string($con,$Row[35]);    } 

    $AFP ="";        
	if(isset($Row[34])) { 
		$AFP = mysqli_real_escape_string($con,$Row[36]);    }         

    $residencia ="";       
	if(isset($Row[35])) { 
		$residencia = mysqli_real_escape_string($con,$Row[37]);    } 

    $barrioResidencia ="";        
	if(isset($Row[38])) { 
		$barrioResidencia = mysqli_real_escape_string($con,$Row[38]);    } 

    $direccionResidencia ="";       
	if(isset($Row[39])) { 
		$direccionResidencia = mysqli_real_escape_string($con,$Row[39]);    } 

    $tipoPerfil ="";        
	if(isset($Row[40])) { 
		$tipoPerfil = mysqli_real_escape_string($con,$Row[40]);    } 

    $tipoTrabajo ="";        
	if(isset($Row[41])) { 
		$tipoTrabajo = mysqli_real_escape_string($con,$Row[41]);    } 

    $poblacionVulnerable ="";        
	if(isset($Row[42])) { 
		$poblacionVulnerable = mysqli_real_escape_string($con,$Row[42]);    } 

//---------------------------------------------------------------------------------------------------
                
if ( !empty($numDocumento) || !empty($tipoDocumento) || !empty($nombre)  || !empty($fechadeExpedicion) || !empty($lugarExpedicion) || !empty($fechaNacimiento) || !empty($lugarNacimiento) || !empty($edad) || !empty($sexo) || !empty($nacionalidad) || !empty($estadoContrato) || !empty($cargo) || !empty($fechaIngresoALaborar) || !empty($fechaTerminacionContrato ) || !empty($tipoTerminacionContrato ) || !empty($fechaEntregaCarnet) || !empty($fechaEntregaDotacion ) || !empty($entidadBancaria) || !empty($certificacionBancaria ) || !empty($fechaAfilacionARL) || !empty($ARL) || !empty($fechaAfilacionEPS) || !empty($EPS) || !empty($residencia) || !empty($barrioResidencia) || !empty($direccionResidencia) || !empty($tipoPerfil) || !empty($tipoTrabajo) || !empty($poblacionVulnerable) ){
                   
                    $contrato=new Contrato($item, $nombre, $tipoDocumento, $numDocumento, $cargo, $telefono1, $telefono2, $correo, $salario, $tipoContrato, $estadoContrato, $proyecto, $fechaInicioProceso, $fechaEnvioConsorcio, $fechaPreAprobado, $fechaEnvioInterventoria, $fechaRespuestaInterventoria, $fechaIngresoALaborar, $fechaTerminacionContrato, $tipoTerminacionContrato, $fechaEntregaCarnet, $fechaEntregaDotacion, $entidadBancaria, $certificacionBancaria, $sexo, $edad, $fechadeExpedicion, $lugarExpedicion, $fechaNacimiento, $lugarNacimiento, $nacionalidad, $fechaAfilacionARL, $ARL, $fechaAfilacionEPS, $EPS, $fechaAfilacionCCF, $AFP, $residencia, $barrioResidencia, $direccionResidencia, $tipoPerfil, $tipoTrabajo, $poblacionVulnerable);
                    
                    $resultados = $contrato -> agregar();
                    if (! empty($resultados)) {
                        $type = "success";
                        $message = "Excel importado correctamente";
                    } else {
                        $type = "error";
                        $message = "Hubo un problema al importar registros";
                    }
                }
             }
        
         }
  unlink($targetPath);      
  }
  else
  { 
        $type = "error";
        $message = "El archivo enviado es invalido. Por favor vuelva a intentarlo";
  }

  
}

?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Custom styles for this template -->
<link href="assets/sticky-footer-navbar.css" rel="stylesheet">
<link href="assets/style.css" rel="stylesheet">

</head>

<body>
<header> 
  <!-- Fixed navbar -->
 

</header>

<!-- Begin page content -->

<div class="container">
  <h3 class="mt-5">Importar archivo Matriz Contratacion de Excel a Dase de Datos Sutec</h3>
  <hr>
  <div class="row">
    <div class="col-12 col-md-12"> 
      <!-- Contenido -->
    
    <div class="outer-container">
        <form action="" method="post"
            name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
            <div>
                <label>Elija Archivo Excel</label> <input type="file" name="file"
                    id="file" accept=".xls,.xlsx">
                <button type="submit" id="submit" name="import"
                    class="btn-submit">Importar Registros</button>
        
            </div>
        
        </form>
        
    </div>
    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    
         

    </div>
  </div>
  <!-- Fin row --> 

  
</div>

<script src="assets/jquery-1.12.4-jquery.min.js"></script> 
<script src="dist/js/bootstrap.min.js"></script>

</body>
</html>