
<?php 


	$cantidad = 1;
	if (isset($_GET["cantidad"])) {
		$cantidad = $_GET["cantidad"];
	}

	$pagina = 1;
	if (isset($_GET["pagina"])) {
		$pagina = $_GET["pagina"];
	}

	$contrato = new Contrato();
	$contratos = $contrato -> consultarPaginacion($cantidad, $pagina);	

	$rol = "";
	if (isset($_GET["rol"])) {
		$rol = $_GET["rol"];
	}	
	
	$totalRegistros = $contrato->consultarCantidad();
	$totalPaginas = intval($totalRegistros / $cantidad);
	
	if ($totalRegistros % $cantidad != 0) {
	
		$totalPaginas++;
	}
	$ultimaPagina = ($totalPaginas == $pagina);	



 ?>

 
<div class="row">
	<div class="col-sm-12">
	<h2>Contratacion</h2>
		<table class="table table-hover table-condensed table-bordered" id="tablaDinamicaLoad">
		<?php if($rol!="CONSULTOR"){ ?>					
		<caption>
			<button class="btn btn-primary" data-toggle="modal" data-target="#modalNuevo">
				Agregar nuevo 
				<span class="glyphicon glyphicon-plus"></span>
			</button>
		</caption> 
		<?php }?>				
			<thead>
				<tr> 
					<td class='table-header' >Acciones</td>										
					<th class="table-header">Apellidos y Nombres</th>
					<th class="table-header">Tipo Docuento</th>
					<th class="table-header">N° Documeto</th>
					<th class="table-header">Cargo a Desempeñar</th>
					<th class="table-header">Telefono1</th>
	  				<th class="table-header">Telefono2</th>
					<th class="table-header">Correo</th>
					<th class="table-header">Tipo del contrato</th>
					<th class="table-header">Estado contrato</th>
					<th class="table-header">Proyecto</th>

				</tr>
			</thead>
			<tbody>
			<?php 

			foreach($contratos as $contratosActual) {	
				
			 ?>

			<tr class="<?php echo ($contratosActual -> getEstadoContrato() == 'APROBADO')? 'success':''?>">
			
				<td class="ajax-action-links">
					<button class="btn btn-info glyphicon glyphicon-eye-open" data-toggle="modal" data-target="#modalVista" onclick="agregaformV('<?php echo $contratosActual ->datos() ?>')"> </button>
					<?php if($rol!="CONSULTOR"){ ?>					
					<button class="btn btn-warning glyphicon glyphicon-pencil" data-toggle="modal" data-target="#modalEdicion" onclick="agregaform('<?php echo $contratosActual ->datos() ?>')"> </button>
					<?php if($rol=="ADMINISTRADOR"){ ?> <button class="btn btn-danger glyphicon glyphicon-remove" onclick="preguntarSiNo('<?php echo $contratosActual ->getItem() ?>')">	</button> <?php }?>			
					<?php }?>			
				</td>
			
				
				<td><?php echo $contratosActual ->getNombre() ?></td>
				<td><?php echo $contratosActual ->getTipoDocumento() ?></td>
				<td><?php echo $contratosActual ->getNumDocumento() ?></td>
				<td><?php echo $contratosActual ->getCargo() ?></td>
				<td><?php echo $contratosActual ->getTelefono1() ?></td>
				<td><?php echo $contratosActual ->getTelefono2() ?></td>
				<td><?php echo $contratosActual ->getCorreo() ?></td>
				<td><?php echo $contratosActual ->getTipoContrato() ?></td>
				<td><?php echo $contratosActual ->getEstadoContrato() ?></td>
				<td><?php echo $contratosActual ->getProyecto() ?></td>
				
			</tr>
			<?php 
		}
			 ?>
		</tbody>
		</table>
		<div class="container text-left lead">
		
		<h5> Resultados <?php echo (($pagina - 1) * $cantidad + 1) ?> al <?php echo (($pagina - 1) * $cantidad) + count($contratos) ?> de <?php echo $totalRegistros ?> </h5>

			<select id="cantidad">
                    <option value="10" <?php echo ($cantidad == 10) ? "selected" : "" ?>>10</option>
                    <option value="20" <?php echo ($cantidad == 20) ? "selected" : "" ?>>20</option>
                    <option value="50" <?php echo ($cantidad == 50) ? "selected" : "" ?>>50</option>
                    <option value="100" <?php echo ($cantidad == 100) ? "selected" : "" ?>>100</option>					
        	</select>

		</div>

			<div class="text-center">
                    <nav>
                        <ul class="pagination">
                            <li class="page-item <?php echo ($pagina == 1) ? "disabled" : ""; ?>"><a class="page-link"  <?php echo ($pagina == 1) ? "" : "onClick='pagina(" . ($pagina - 1) . "," . $cantidad . ")'"; ?> ><i class="fas fa-angle-double-left"></i></a></li>
                            <?php
                            for ($i = 1; $i <= $totalPaginas; $i++) {
                                if ($i == $pagina) {
                                    echo "<li class='page-item active' aria-current='page'><span class='page-link'>" . $i . "<span class='sr-only'></span></span></li>";
                                } else {
                                    echo "<li class='page-item'><a class='page-link'  onClick='pagina(" . $i . "," . $cantidad . ")'>" . $i . "</a></li>";
                                }
                            }
                            ?>
                            <li class="page-item <?php echo ($ultimaPagina) ? "disabled" : ""; ?>"><a class="page-link"  <?php echo ($ultimaPagina) ? "" : "onClick='pagina(" . ($pagina + 1) . "," . $cantidad . ")'"; ?>  ><i class="fas fa-angle-double-right"></i></a></li>				
                        </ul>
                    </nav>
                </div>                
            </div>

	</div>
</div>

<script>

$("#cantidad").on("change", function() {

	cambiarCantida($("#cantidad").val());

});

</script>
