<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>


<input hidden="" type="text" name="rol" id="rol" value="<?php echo $_SESSION["rol"]?>">

<script src="presentacion/contrato2/js/funcionesContrato.js"></script>
<link rel="stylesheet" type="text/css" href="presentacion/contrato2/librerias/alertifyjs/css/alertify.css">
<link rel="stylesheet" type="text/css" href="presentacion/contrato2/librerias/alertifyjs/css/themes/default.css">
<script src="presentacion/contrato2/librerias/alertifyjs/alertify.js"></script>


</head>
<body style="padding-bottom:100">

    <div class="container"> <img src="img/logo.png" class="img-fluid" alt="Responsive image"> </div>



	<div class="container">
		<div id="tabla"></div>
	</div>

	<!-- Modal para registros nuevos -->


<div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agrega nueva persona</h4>
      </div>
      <div class="modal-body" style="height: 500px !important; overflow-y: scroll;">
        	
        <!--<label>item</label>    <input type="text" name="" id="item" class="form-control input-sm">-->
        <label style="margin-top:10px;">Apellidos y Nombres</label>    <input type="text" name="" id="nombre" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Documento</label>    <input type="text" name="" id="tipoDocumento" class="form-control input-sm">
        <label style="margin-top:10px;">N° Documento</label>    <input type="text" name="" id="numDocumento" class="form-control input-sm">
        <label style="margin-top:10px;">cargo a desempeñar</label>    <input type="text" name="" id="cargo" class="form-control input-sm">
        <label style="margin-top:10px;">telefono 1</label>    <input type="text" name="" id="telefono1" class="form-control input-sm">
        <label style="margin-top:10px;">telefono 2</label>    <input type="text" name="" id="telefono2" class="form-control input-sm">
        <label style="margin-top:10px;">correo</label>    <input type="text" name="" id="correo" class="form-control input-sm">
        <label style="margin-top:10px;">salario</label>    <input type="text" name="" id="salario" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Contrato</label>    <input type="text" name="" id="tipoContrato" class="form-control input-sm">        
        <!--<label style="margin-top:10px;">Observación (Como va el proceso)</label>    <input type="text" name="" id="estadoContrato" class="form-control input-sm">-->
        <label for="estadoContrato">Observación (Como va el proceso)</label>
              <select id="estadoContrato" class="form-control">
              <option value=" "> OPCIONES</option>
                <option value="APROBADO">APROBADO</option>
                <option value="NO APTO">NO APTO</option>
                <option value="PRE APROBADO">PRE APROBADO</option>
                <option value="DESISTIO DEL PROCESO">DESISTIO DEL PROCESO</option>
              </select>        
        <label style="margin-top:10px;">proyecto</label>    <input type="text" name="" id="proyecto" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Inicio Proceso</label>    <input type="date" name="" id="fechaInicioProceso" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Envio Consorcio</label>    <input type="date" name="" id="fechaEnvioConsorcio" class="form-control input-sm">
        <label style="margin-top:10px;">fecha PreAprobado</label>    <input type="date" name="" id="fechaPreAprobado" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Envio Interventoria</label>    <input type="date" name="" id="fechaEnvioInterventoria" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Respuesta Interventoria</label>    <input type="date" name="" id="fechaRespuestaInterventoria" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Ingreso ALaborar</label>    <input type="date" name="" id="fechaIngresoALaborar" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Terminacion Contrato</label>    <input type="date" name="" id="fechaTerminacionContrato" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Terminacion Contrato</label>    <input type="text" name="" id="tipoTerminacionContrato" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Entrega Carnet</label>    <input type="date" name="" id="fechaEntregaCarnet" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Entrega Dotación</label>    <input type="date" name="" id="fechaEntregaDotacion" class="form-control input-sm">
        <label style="margin-top:10px;">entidad Bancaria</label>    <input type="text" name="" id="entidadBancaria" class="form-control input-sm">
        <label style="margin-top:10px;">certificacion Bancaria</label>    <input type="text" name="" id="certificacionBancaria" class="form-control input-sm">
        <label style="margin-top:10px;">sexo</label>    <input type="text" name="" id="sexo" class="form-control input-sm">
        <label style="margin-top:10px;">edad</label>    <input type="text" name="" id="edad" class="form-control input-sm">
        <label style="margin-top:10px;">fechade Expedicion</label>    <input type="date" name="" id="fechadeExpedicion" class="form-control input-sm">
        <label style="margin-top:10px;">lugar Expedicion</label>    <input type="text" name="" id="lugarExpedicion" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Nacimiento</label>    <input type="date" name="" id="fechaNacimiento" class="form-control input-sm">
        <label style="margin-top:10px;">lugar Nacimiento</label>    <input type="text" name="" id="lugarNacimiento" class="form-control input-sm">            
        <label style="margin-top:10px;">nacionalidad</label>    <input type="text" name="" id="nacionalidad" class="form-control input-sm">        
        <label style="margin-top:10px;">fecha Afilacion ARL</label>    <input type="date" name="" id="fechaAfilacionARL" class="form-control input-sm">
        <label style="margin-top:10px;">ARL</label>    <input type="text" name="" id="ARL" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Afilacion EPS</label>    <input type="date" name="" id="fechaAfilacionEPS" class="form-control input-sm">
        <label style="margin-top:10px;">EPS</label>    <input type="text" name="" id="EPS" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Afilacion CCF</label>    <input type="date" name="" id="fechaAfilacionCCF" class="form-control input-sm">        
        <label style="margin-top:10px;">AFP</label>    <input type="text" name="" id="AFP" class="form-control input-sm">
        <label style="margin-top:10px;">Localidad y/o municipio (fuera de Bogotá) de residencia</label>    <input type="text" name="" id="residencia" class="form-control input-sm">
        <label style="margin-top:10px;">barrio Residencia</label>    <input type="text" name="" id="barrioResidencia" class="form-control input-sm">
        <label style="margin-top:10px;">direccion completa Residencia</label>    <input type="text" name="" id="direccionResidencia" class="form-control input-sm">
        <label style="margin-top:10px;">indicar el tipo de Perfil</label>    <input type="text" name="" id="tipoPerfil" class="form-control input-sm">
        <label style="margin-top:10px;">tipo de Trabajo</label>    <input type="text" name="" id="tipoTrabajo" class="form-control input-sm">
        <label style="margin-top:10px;">¿Pertenece a población especial o vulnerable certificada?</label>    <input type="text" name="" id="poblacionVulnerable" class="form-control input-sm">

            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="guardarnuevo">
        Agregar
        </button>
       
      </div>
    </div>
  </div>
</div>

<!-- Modal para edicion de datos -->

<div class="modal fade" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Actualizar datos</h4>
      </div>
      <div class="modal-body" style="height: 500px !important; overflow-y: scroll;">  
      		
        <input hidden="" type="text" name="" id="itemu">
        <label style="margin-top:10px;">Apellidos y Nombres</label>    <input type="text" name="" id="nombreu" class="form-control input-sm" onBlur="saveToDatabase(this,'nombre')" >
        <label style="margin-top:10px;">tipo Documento</label>    <input type="text" name="" id="tipoDocumentou" class="form-control input-sm" onBlur="saveToDatabase(this,'tipoDocumento')">
        <label style="margin-top:10px;">N° Documento</label>    <input type="text" name="" id="numDocumentou" class="form-control input-sm"  onBlur="saveToDatabase(this,'numDocumento')"  >
        <label style="margin-top:10px;">cargo a desempeñar</label>    <input type="text" name="" id="cargou" class="form-control input-sm"  onBlur="saveToDatabase(this,'cargo')"  >
        <label style="margin-top:10px;">telefono 1</label>    <input type="text" name="" id="telefono1u" class="form-control input-sm"  onBlur="saveToDatabase(this,'telefono1')"  >
        <label style="margin-top:10px;">telefono 2</label>    <input type="text" name="" id="telefono2u" class="form-control input-sm"  onBlur="saveToDatabase(this,'telefono2')"  >
        <label style="margin-top:10px;">correo</label>    <input type="text" name="" id="correou" class="form-control input-sm"  onBlur="saveToDatabase(this,'correo')"  >
        <label style="margin-top:10px;">salario</label>    <input type="text" name="" id="salariou" class="form-control input-sm"  onBlur="saveToDatabase(this,'salario')"  >
        <label style="margin-top:10px;">tipo Contrato</label>    <input type="text" name="" id="tipoContratou" class="form-control input-sm"  onBlur="saveToDatabase(this,'tipoContrato')"  >                
        <label for="estadoContratou">Observación (Como va el proceso)</label>
              <select id="estadoContratou" class="form-control" onChange="saveToDatabase(this,'estadoContrato')">              
                <option value="APROBADO">APROBADO</option>
                <option value="NO APTO">NO APTO</option>
                <option value="PRE APROBADO">PRE APROBADO</option>
                <option value="DESISTIO DEL PROCESO">DESISTIO DEL PROCESO</option>
              </select>
        
        <label style="margin-top:10px;">proyecto</label>    <input type="text" name="" id="proyectou" class="form-control input-sm"  onBlur="saveToDatabase(this,'proyecto')"  >
        <label style="margin-top:10px;">fecha Inicio Proceso</label>    <input type="date" name="" id="fechaInicioProcesou" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaInicioProceso')"  >
        <label style="margin-top:10px;">fecha Envio Consorcio</label>    <input type="date" name="" id="fechaEnvioConsorciou" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaEnvioConsorcio')"  >
        <label style="margin-top:10px;">fecha PreAprobado</label>    <input type="date" name="" id="fechaPreAprobadou" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaPreAprobado')"  >
        <label style="margin-top:10px;">fecha Envio Interventoria</label>    <input type="date" name="" id="fechaEnvioInterventoriau" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaEnvioInterventoria')"  >
        <label style="margin-top:10px;">fecha Respuesta Interventoria</label>    <input type="date" name="" id="fechaRespuestaInterventoriau" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaRespuestaInterventoria')"  >
        <label style="margin-top:10px;">fecha Ingreso ALaborar</label>    <input type="date" name="" id="fechaIngresoALaboraru" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaIngresoALaborar')"  >
        <label style="margin-top:10px;">fecha Terminacion Contrato</label>    <input type="date" name="" id="fechaTerminacionContratou" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaTerminacionContrato')"  >
        <label style="margin-top:10px;">tipo Terminacion Contrato</label>    <input type="text" name="" id="tipoTerminacionContratou" class="form-control input-sm"  onBlur="saveToDatabase(this,'tipoTerminacionContrato')"  >
        <label style="margin-top:10px;">fecha Entrega Carnet</label>    <input type="date" name="" id="fechaEntregaCarnetu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaEntregaCarnet')"  >
        
	      <label style="margin-top:10px;">fecha Entrega Dotación</label>    <input type="date" name="" id="fechaEntregaDotacionu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaEntregaDotacion')"  >
        <label style="margin-top:10px;">entidad Bancaria</label>    <input type="text" name="" id="entidadBancariau" class="form-control input-sm"  onBlur="saveToDatabase(this,'entidadBancaria')"  >
        <label style="margin-top:10px;">certificacion Bancaria</label>    <input type="text" name="" id="certificacionBancariau" class="form-control input-sm"  onBlur="saveToDatabase(this,'certificacionBancaria')"  >
        <label style="margin-top:10px;">sexo</label>    <input type="text" name="" id="sexou" class="form-control input-sm"  onBlur="saveToDatabase(this,'sexo')"  >
        <label style="margin-top:10px;">edad</label>    <input type="text" name="" id="edadu" class="form-control input-sm"  onBlur="saveToDatabase(this,'edad')"  >
        <label style="margin-top:10px;">fechade Expedicion</label>    <input type="date" name="" id="fechadeExpedicionu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechadeExpedicion')"  >
        <label style="margin-top:10px;">lugar Expedicion</label>    <input type="text" name="" id="lugarExpedicionu" class="form-control input-sm"  onBlur="saveToDatabase(this,'lugarExpedicion')"  >
        <label style="margin-top:10px;">fecha Nacimiento</label>    <input type="date" name="" id="fechaNacimientou" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaNacimiento')"  >
        <label style="margin-top:10px;">lugar Nacimiento</label>    <input type="text" name="" id="lugarNacimientou" class="form-control input-sm"  onBlur="saveToDatabase(this,'lugarNacimiento')"  >            
        <label style="margin-top:10px;">nacionalidad</label>    <input type="text" name="" id="nacionalidadu" class="form-control input-sm"  onBlur="saveToDatabase(this,'nacionalidad')"  >        
        
	      <label style="margin-top:10px;">fecha Afilacion ARL</label>    <input type="date" name="" id="fechaAfilacionARLu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaAfilacionARL')"  >
        <label style="margin-top:10px;">ARL</label>    <input type="text" name="" id="ARLu" class="form-control input-sm"  onBlur="saveToDatabase(this,'ARL')"  >
        <label style="margin-top:10px;">fecha Afilacion EPS</label>    <input type="date" name="" id="fechaAfilacionEPSu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaAfilacionEPS')"  >
        <label style="margin-top:10px;">EPS</label>    <input type="text" name="" id="EPSu" class="form-control input-sm"  onBlur="saveToDatabase(this,'EPS')"  >
        <label style="margin-top:10px;">fecha Afilacion CCF</label>    <input type="date" name="" id="fechaAfilacionCCFu" class="form-control input-sm"  onBlur="saveToDatabase(this,'fechaAfilacionCCF')"  >        
        <label style="margin-top:10px;">AFP</label>    <input type="text" name="" id="AFPu" class="form-control input-sm"  onBlur="saveToDatabase(this,'AFP')"  >
        <label style="margin-top:10px;">Localidad y/o municipio (fuera de Bogotá) de residencia</label>    <input type="text" name="" id="residenciau" class="form-control input-sm"  onBlur="saveToDatabase(this,'residencia')"  >
        <label style="margin-top:10px;">barrio Residencia</label>    <input type="text" name="" id="barrioResidenciau" class="form-control input-sm"  onBlur="saveToDatabase(this,'barrioResidencia')"  >
        <label style="margin-top:10px;">direccion completa Residencia</label>    <input type="text" name="" id="direccionResidenciau" class="form-control input-sm"  onBlur="saveToDatabase(this,'direccionResidencia')"  >
        <label style="margin-top:10px;">indicar el tipo de Perfil</label>    <input type="text" name="" id="tipoPerfilu" class="form-control input-sm"  onBlur="saveToDatabase(this,'tipoPerfil')"  >
        
	      <label style="margin-top:10px;">tipo de Trabajo</label>    <input type="text" name="" id="tipoTrabajou" class="form-control input-sm"  onBlur="saveToDatabase(this,'tipoTrabajo')"  >
        <label style="margin-top:10px;">¿Pertenece a población especial o vulnerable certificada?</label>    <input type="text" name="" id="poblacionVulnerableu" class="form-control input-sm"  onBlur="saveToDatabase(this,'poblacionVulnerable')"  >


      </div> 
      <div class="modal-footer">     
        <button type="button" class="btn btn-primary" data-dismiss="modal" id="actualizarFormulario">Actualizar Formulario</button>
      </div> 
    </div>
  </div>
</div>




<!-- Modal para VER datos de datos -->

<div class="modal fade" id="modalVista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ver Detalles</h4>
      </div>
      <div class="modal-body" style="height: 500px !important; overflow-y: scroll;">        		
        
        <input hidden="" type="text" name="" id="itemv">
        <label style="margin-top:10px;">Apellidos y Nombres</label>    <input disabled type="text" name="" id="nombrev" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Documento</label>    <input disabled type="text" name="" id="tipoDocumentov" class="form-control input-sm">
        <label style="margin-top:10px;">N° Documento</label>    <input disabled type="text" name="" id="numDocumentov" class="form-control input-sm">
        <label style="margin-top:10px;">cargo a desempeñar</label>    <input disabled type="text" name="" id="cargov" class="form-control input-sm">
        <label style="margin-top:10px;">telefono 1</label>    <input disabled type="text" name="" id="telefono1v" class="form-control input-sm">
        <label style="margin-top:10px;">telefono 2</label>    <input disabled type="text" name="" id="telefono2v" class="form-control input-sm">
        <label style="margin-top:10px;">correo</label>    <input disabled type="text" name="" id="correov" class="form-control input-sm">
        <label style="margin-top:10px;">salario</label>    <input disabled type="text" name="" id="salariov" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Contrato</label>    <input disabled type="text" name="" id="tipoContratov" class="form-control input-sm">        
        <label style="margin-top:10px;">Observación (Como va el proceso)</label>    <input disabled type="text" name="" id="estadoContratov" class="form-control input-sm">
        <label style="margin-top:10px;">proyecto</label>    <input disabled type="text" name="" id="proyectov" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Inicio Proceso</label>    <input disabled type="text" name="" id="fechaInicioProcesov" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Envio Consorcio</label>    <input disabled type="text" name="" id="fechaEnvioConsorciov" class="form-control input-sm">
        <label style="margin-top:10px;">fecha PreAprobado</label>    <input disabled type="text" name="" id="fechaPreAprobadov" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Envio Interventoria</label>    <input disabled type="text" name="" id="fechaEnvioInterventoriav" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Respuesta Interventoria</label>    <input disabled type="text" name="" id="fechaRespuestaInterventoriav" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Ingreso ALaborar</label>    <input disabled type="text" name="" id="fechaIngresoALaborarv" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Terminacion Contrato</label>    <input disabled type="text" name="" id="fechaTerminacionContratov" class="form-control input-sm">
        <label style="margin-top:10px;">tipo Terminacion Contrato</label>    <input disabled type="text" name="" id="tipoTerminacionContratov" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Entrega Carnet</label>    <input disabled type="text" name="" id="fechaEntregaCarnetv" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Entrega Dotación</label>    <input disabled type="text" name="" id="fechaEntregaDotacionv" class="form-control input-sm">
        <label style="margin-top:10px;">entidad Bancaria</label>    <input disabled type="text" name="" id="entidadBancariav" class="form-control input-sm">
        <label style="margin-top:10px;">certificacion Bancaria</label>    <input disabled type="text" name="" id="certificacionBancariav" class="form-control input-sm">
        <label style="margin-top:10px;">sexo</label>    <input disabled type="text" name="" id="sexov" class="form-control input-sm">
        <label style="margin-top:10px;">edad</label>    <input disabled type="text" name="" id="edadv" class="form-control input-sm">
        <label style="margin-top:10px;">fechade Expedicion</label>    <input disabled type="date" name="" id="fechadeExpedicionv" class="form-control input-sm">
        <label style="margin-top:10px;">lugar Expedicion</label>    <input disabled type="text" name="" id="lugarExpedicionv" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Nacimiento</label>    <input disabled type="text" name="" id="fechaNacimientov" class="form-control input-sm">
        <label style="margin-top:10px;">lugar Nacimiento</label>    <input disabled type="text" name="" id="lugarNacimientov" class="form-control input-sm">            
        <label style="margin-top:10px;">nacionalidad</label>    <input disabled type="text" name="" id="nacionalidadv" class="form-control input-sm">        
        <label style="margin-top:10px;">fecha Afilacion ARL</label>    <input disabled type="text" name="" id="fechaAfilacionARLv" class="form-control input-sm">
        <label style="margin-top:10px;">ARL</label>    <input disabled type="text" name="" id="ARLv" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Afilacion EPS</label>    <input disabled type="date" name="" id="fechaAfilacionEPSv" class="form-control input-sm">
        <label style="margin-top:10px;">EPS</label>    <input disabled type="text" name="" id="EPSv" class="form-control input-sm">
        <label style="margin-top:10px;">fecha Afilacion CCF</label>    <input disabled type="text" name="" id="fechaAfilacionCCFv" class="form-control input-sm">        
        <label style="margin-top:10px;">AFP</label>    <input disabled type="text" name="" id="AFPv" class="form-control input-sm">
        <label style="margin-top:10px;">Localidad y/o municipio (fuera de Bogotá) de residencia</label>    <input disabled type="text" name="" id="residenciav" class="form-control input-sm">
        <label style="margin-top:10px;">barrio Residencia</label>    <input disabled type="text" name="" id="barrioResidenciav" class="form-control input-sm">
        <label style="margin-top:10px;">direccion completa Residencia</label>    <input disabled type="text" name="" id="direccionResidenciav" class="form-control input-sm">
        <label style="margin-top:10px;">indicar el tipo de Perfil</label>    <input disabled type="text" name="" id="tipoPerfilv" class="form-control input-sm">
        <label style="margin-top:10px;">tipo de Trabajo</label>    <input disabled type="text" name="" id="tipoTrabajov" class="form-control input-sm">
        <label style="margin-top:10px;">¿Pertenece a población especial o vulnerable certificada?</label>    <input disabled type="text" name="" id="poblacionVulnerablev" class="form-control input-sm">

      </div>      
    </div>
  </div>
</div>




</body>
</html>

<script type="text/javascript">
  
    var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/contrato2/componentes/tabla.php")?>&rol="+$('#rol').val();
    $("#tabla").load(url);
   
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#guardarnuevo').click(function(){
          
           agregardatos($('#nombre').val(),$('#tipoDocumento').val(),$('#numDocumento').val(),$('#cargo').val(),$('#telefono1').val(),$('#telefono2').val(),$('#correo').val(),$('#salario').val(),$('#tipoContrato').val(),$('#estadoContrato').val(),$('#proyecto').val(),$('#fechaInicioProceso').val(),$('#fechaEnvioConsorcio').val(),$('#fechaPreAprobado').val(),$('#fechaEnvioInterventoria').val(),$('#fechaRespuestaInterventoria').val(),$('#fechaIngresoALaborar').val(),$('#fechaTerminacionContrato').val(),$('#tipoTerminacionContrato').val(),$('#fechaEntregaCarnet').val(),$('#fechaEntregaDotacion').val(),$('#entidadBancaria').val(),$('#certificacionBancaria').val(),$('#sexo').val(),$('#edad').val(),$('#fechadeExpedicion').val(),$('#lugarExpedicion').val(),$('#fechaNacimiento').val(),$('#lugarNacimiento').val(),$('#nacionalidad').val(),$('#fechaAfilacionARL').val(),$('#ARL').val(),$('#fechaAfilacionEPS').val(),$('#EPS').val(),$('#fechaAfilacionCCF').val(),$('#AFP').val(),$('#residencia').val(),$('#barrioResidencia').val(),$('#direccionResidencia').val(),$('#tipoPerfil').val(),$('#tipoTrabajo').val(),$('#poblacionVulnerable').val());
           //alertify.error(":("+$('#fechaEntregaDotacion').val());
        });

        $('#actualizarFormulario').click(function(){
          var url = "indexAjax.php?pid=<?php echo base64_encode('presentacion/contrato2/componentes/tabla.php')?>&rol="+$('#rol').val();
          $("#tabla").load(url);

        });

    });


    function editRow(editableObj) {
      $(editableObj).css("background","#FFF url(cargando.gif) no-repeat right");
}

function saveToDatabase(editableObj,column) {
	//alert('column='+column+'&editval='+$(editableObj).text()+'&id='+id);

  $(editableObj).css("background","#FFF url(cargando.gif) no-repeat right");
  newVal="";
 	      
  idd="#"+column+"u";
  //alertify.error(":("+column+newVal+$(idd).val());
  actualizaDatosEditarColumn(editableObj,$(idd).val(),column,$('#itemu').val());

  
}

</script>

