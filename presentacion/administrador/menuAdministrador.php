<nav class="navbar navbar-default">
<div class="container-fluid">

<!-- BRAND -->
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alignment-example" aria-expanded="false">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/sesion.php"); ?>">ADMINISTRADOR</a>
</div>

<!-- COLLAPSIBLE NAVBAR -->
<div class="collapse navbar-collapse" id="alignment-example">

<!-- Links -->
<ul class="nav navbar-nav">
<li><a  href="index.php?pid=<?php echo base64_encode("presentacion/usuarios/usuarios.php"); ?>">Usuarios</a></li>

<!--<li><a href="#">Link 3</a></li>-->
 <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contratos
        <span class="caret"></span></a>
        <ul class="dropdown-menu">            
            <li><a href="index.php?pid=<?php echo base64_encode("presentacion/contrato2/contrato.php"); ?>">Consultar Registros</a></li>
            <li><a href="index.php?pid=<?php echo base64_encode("presentacion/contrato2/subirExcel.php"); ?>">Subir Excel</a></li>            
        </ul>
      </li>
</ul>

<!-- Search (left) -->
<!--
<ul class="nav navbar-nav navbar-ringt">
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About Us <span class="caret"></span></a>
<ul class="dropdown-menu" aria-labelledby="about-us">
<li><a href="#">Our Story</a></li>
<li><a href="#">Our Team</a></li>
<li><a href="#">Contact Us</a></li>
</ul>
</li>
</ul>
-->
<!-- Link (right) -->
<ul class="nav navbar-nav navbar-right">
<li><a href="index.php?pid=<?php echo base64_encode("presentacion/sesion.php"); ?>"> <span class="fas fa-user-alt"></span>
                <?php echo ($_SESSION["nombre"] != "") ? $_SESSION["nombre"] : $_SESSION["correo"];?></a></li>
<li> <a class="nav-link" href="index.php?cerrarSesion=true"><span class="fas fa-sign-in-alt"></span> Cerrar Sesion</a> </li>
</ul>

</div>

</div>
</nav>