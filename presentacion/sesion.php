<div class="container mt-3" style="padding-top:60">
	<div class="row">
     <div class="col-lg-5"></div>
    <div class="col-lg-3">    <img src="img/logo.png" class="img-fluid" alt="Responsive image">     </div>
    </div>
</div>

<div class="container mt-3" style="padding-top:20">
	<div class="row">
        <div class="col-lg-3"></div>
		<div class="col-lg-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4>Bienvenido <?php echo ($_SESSION["nombre"] != "") ? $_SESSION["nombre"] : $_SESSION["correo"];?></h4>
				</div>
				<div class="panel-body">
											
						<div class="col-9">
							<table class="table table-hover">
								<tr>
									<th>Nombre</th>
									<td><?php echo $_SESSION["nombre"] ?></td>
								</tr>
								<tr>
									<th>Apellido</th>
									<td><?php echo $_SESSION["apellido"] ?></td>
								</tr>
								<tr>
									<th>Correo</th>
									<td><?php echo $_SESSION["correo"] ?></td>
								</tr>
							</table>
						</div>
					
				</div>
				
			</div>
		</div>		
	</div>
</div>