<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ContratoDAO.php";
class Contrato{
    private $item;
    private $nombre;
    private $tipoDocumento;
    private $numDocumento;
    private $cargo;
    private $telefono1;
    private $telefono2;
    private $correo;
    private $salario;
    private $tipoContrato;
    private $estadoContrato;
    private $proyecto;
    private $fechaInicioProceso;
    private $fechaEnvioConsorcio;
    private $fechaPreAprobado;
    private $fechaEnvioInterventoria;
    private $fechaRespuestaInterventoria;
    private $fechaIngresoALaborar;
    private $fechaTerminacionContrato;
    private $tipoTerminacionContrato;
    private $fechaEntregaCarnet;
    private $fechaEntregaDotacion;
    private $entidadBancaria;
    private $certificacionBancaria;
    private $sexo;
    private $edad;
    private $fechadeExpedicion;
    private $lugarExpedicion;
    private $fechaNacimiento;
    private $lugarNacimiento;
    private $nacionalidad;
    private $fechaAfilacionARL;
    private $ARL;
    private $fechaAfilacionEPS;
    private $EPS;
    private $fechaAfilacionCCF;
    private $AFP;
    private $residencia;
    private $barrioResidencia;
    private $direccionResidencia;
    private $tipoPerfil;
    private $tipoTrabajo;
    private $poblacionVulnerable;


    private $conexion;
    private $contratoDAO;

    public function getItem(){    return $this -> item;    }
    public function getNombre(){        return $this -> nombre;    }
    public function getTipoDocumento(){        return $this -> tipoDocumento;    }
    public function getNumDocumento(){        return $this -> numDocumento;}
    public function getCargo(){    return $this -> cargo; }
    public function getTelefono1(){    return $this -> telefono1; }
    public function getTelefono2(){    return $this -> telefono2; }
    public function getCorreo(){    return $this -> correo; }
    public function getSalario(){    return $this -> salario; }
    public function getTipoContrato(){    return $this -> tipoContrato; }
    public function getEstadoContrato(){    return $this -> estadoContrato; }
    public function getProyecto(){    return $this -> proyecto; }
    public function getFechaInicioProceso(){        return $this -> fechaInicioProceso;   }
    public function getFechaEnvioConsorcio(){        return $this -> fechaEnvioConsorcio;   }
    public function getFechaPreAprobado(){        return $this -> fechaPreAprobado;   }
    public function getFechaEnvioInterventoria(){        return $this -> fechaEnvioInterventoria;   }
    public function getFechaRespuestaInterventoria(){        return $this -> fechaRespuestaInterventoria;   }
    public function getFechaIngresoALaborar(){    return $this -> fechaIngresoALaborar; }
    public function getFechaTerminacionContrato(){    return $this -> fechaTerminacionContrato ; }
    public function getTipoTerminacionContrato(){    return $this -> tipoTerminacionContrato ; }
    public function getfechaEntregaCarnet(){    return $this -> fechaEntregaCarnet; }
    public function getFechaEntregaDotacion(){    return $this -> fechaEntregaDotacion ; }
    public function getEntidadBancaria(){    return $this -> entidadBancaria; }
    public function getCertificacionBancaria(){    return $this -> certificacionBancaria ; }
    public function getSexo(){    return $this -> sexo; }
    public function getEdad(){    return $this -> edad; }
    public function getFechadeExpedicion(){        return $this -> fechadeExpedicion;   }
    public function getLugarExpedicion(){    return $this -> lugarExpedicion; }
    public function getFechaNacimiento(){    return $this -> fechaNacimiento; }
    public function getLugarNacimiento(){    return $this -> lugarNacimiento; }
    public function getNacionalidad(){    return $this -> nacionalidad; }
    
    public function getFechaAfilacionARL(){    return $this -> fechaAfilacionARL; }
    public function getARL(){    return $this -> ARL; }
    public function getFechaAfilacionEPS(){    return $this -> fechaAfilacionEPS; }
    public function getEPS(){    return $this -> EPS; }
    public function getFechaAfilacionCCF(){    return $this -> fechaAfilacionCCF; }
    
    public function getAFP(){    return $this -> AFP; }


    public function getResidencia(){    return $this -> residencia; }
    public function getBarrioResidencia(){    return $this -> barrioResidencia; }
    public function getDireccionResidencia(){    return $this -> direccionResidencia; }
    public function getTipoPerfil(){    return $this -> tipoPerfil; }
    public function getTipoTrabajo(){    return $this -> tipoTrabajo; }
    public function getPoblacionVulnerable(){    return $this -> poblacionVulnerable; }
    
    public function __construct($item ="", $nombre ="", $tipoDocumento ="", $numDocumento ="", $cargo ="", $telefono1 ="", $telefono2 ="", $correo ="", $salario ="", $tipoContrato ="", $estadoContrato ="", $proyecto ="", $fechaInicioProceso ="", $fechaEnvioConsorcio ="", $fechaPreAprobado ="", $fechaEnvioInterventoria ="", $fechaRespuestaInterventoria ="", $fechaIngresoALaborar ="", $fechaTerminacionContrato ="", $tipoTerminacionContrato ="", $fechaEntregaCarnet ="", $fechaEntregaDotacion ="", $entidadBancaria ="", $certificacionBancaria ="", $sexo ="", $edad ="", $fechadeExpedicion ="", $lugarExpedicion ="", $fechaNacimiento ="", $lugarNacimiento ="", $nacionalidad ="", $fechaAfilacionARL ="", $ARL ="", $fechaAfilacionEPS ="", $EPS ="", $fechaAfilacionCCF ="", $AFP ="", $residencia ="", $barrioResidencia ="", $direccionResidencia ="", $tipoPerfil ="", $tipoTrabajo ="", $poblacionVulnerable =""){

        $this -> item = $item;
        $this -> nombre = $nombre;
        $this -> tipoDocumento = $tipoDocumento;
        $this -> numDocumento = $numDocumento;
        $this -> cargo = $cargo;
        $this -> telefono1 = $telefono1;
        $this -> telefono2 = $telefono2;
        $this -> correo = $correo;
        $this -> salario = $salario;
        $this -> tipoContrato = $tipoContrato;
        $this -> estadoContrato = $estadoContrato;
        $this -> proyecto = $proyecto;
        $this -> fechaInicioProceso = $fechaInicioProceso;
        $this -> fechaEnvioConsorcio = $fechaEnvioConsorcio;
        $this -> fechaPreAprobado = $fechaPreAprobado;
        $this -> fechaEnvioInterventoria = $fechaEnvioInterventoria;
        $this -> fechaRespuestaInterventoria = $fechaRespuestaInterventoria;
        $this -> fechaIngresoALaborar = $fechaIngresoALaborar;
        $this -> fechaTerminacionContrato = $fechaTerminacionContrato ;
        $this -> tipoTerminacionContrato = $tipoTerminacionContrato ;
        $this -> fechaEntregaCarnet = $fechaEntregaCarnet;
        $this -> fechaEntregaDotacion = $fechaEntregaDotacion ;
        $this -> entidadBancaria = $entidadBancaria;
        $this -> certificacionBancaria = $certificacionBancaria ;
        $this -> sexo = $sexo;
        $this -> edad = $edad;
        $this -> fechadeExpedicion = $fechadeExpedicion;
        $this -> lugarExpedicion = $lugarExpedicion;
        $this -> fechaNacimiento = $fechaNacimiento;
        $this -> lugarNacimiento = $lugarNacimiento;            
        $this -> nacionalidad = $nacionalidad;        
        $this -> fechaAfilacionARL = $fechaAfilacionARL;
        $this -> ARL = $ARL;
        $this -> fechaAfilacionEPS = $fechaAfilacionEPS;
        $this -> EPS = $EPS;
        $this -> fechaAfilacionCCF = $fechaAfilacionCCF;
        
        $this -> AFP = $AFP;

        $this -> residencia = $residencia;
        $this -> barrioResidencia = $barrioResidencia;
        $this -> direccionResidencia = $direccionResidencia;
        $this -> tipoPerfil = $tipoPerfil;
        $this -> tipoTrabajo = $tipoTrabajo;
        $this -> poblacionVulnerable = $poblacionVulnerable;

        $this -> conexion = new Conexion();
        
        $this -> contratoDAO = new ContratoDAO( $this -> item, $this -> nombre, $this -> tipoDocumento, $this -> numDocumento, $this -> cargo, $this -> telefono1, $this -> telefono2, $this -> correo, $this -> salario, $this -> tipoContrato, $this -> estadoContrato, $this -> proyecto, $this -> fechaInicioProceso, $this -> fechaEnvioConsorcio, $this -> fechaPreAprobado, $this -> fechaEnvioInterventoria, $this -> fechaRespuestaInterventoria, $this -> fechaIngresoALaborar, $this -> fechaTerminacionContrato, $this -> tipoTerminacionContrato, $this -> fechaEntregaCarnet, $this -> fechaEntregaDotacion, $this -> entidadBancaria, $this -> certificacionBancaria, $this -> sexo, $this -> edad, $this -> fechadeExpedicion, $this -> lugarExpedicion, $this -> fechaNacimiento, $this -> lugarNacimiento, $this -> nacionalidad, $this -> fechaAfilacionARL, $this -> ARL, $this -> fechaAfilacionEPS, $this -> EPS, $this -> fechaAfilacionCCF, $this -> AFP, $this -> residencia, $this -> barrioResidencia, $this -> direccionResidencia, $this -> tipoPerfil, $this -> tipoTrabajo, $this -> poblacionVulnerable );

    }
//--------------------------------------------------------------------------------------------------------------

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> item = $resultado[0];
        $this -> nombre = $resultado[1];
        $this -> tipoDocumento = $resultado[2];
        $this -> numDocumento = $resultado[3];
        $this -> cargo = $resultado[4];
        $this -> telefono1 = $resultado[5];
        $this -> telefono2 = $resultado[6];
        $this -> correo = $resultado[7];
        $this -> salario = $resultado[8];
        $this -> tipoContrato = $resultado[9];
        $this -> estadoContrato = $resultado[10];
        $this -> proyecto = $resultado[11];
        $this -> fechaInicioProceso = $resultado[12];
        $this -> fechaEnvioConsorcio = $resultado[13];
        $this -> fechaPreAprobado = $resultado[14];
        $this -> fechaEnvioInterventoria = $resultado[15];
        $this -> fechaRespuestaInterventoria = $resultado[16];
        $this -> fechaIngresoALaborar = $resultado[17];
        $this -> fechaTerminacionContrato = $resultado[18];
        $this -> tipoTerminacionContrato = $resultado[19];
        $this -> fechaEntregaCarnet = $resultado[20];
        $this -> fechaEntregaDotacion = $resultado[21];
        $this -> entidadBancaria = $resultado[22];
        $this -> certificacionBancaria = $resultado[23];
        $this -> sexo = $resultado[24];
        $this -> edad = $resultado[25];
        $this -> fechadeExpedicion = $resultado[26];
        $this -> lugarExpedicion = $resultado[27];
        $this -> fechaNacimiento = $resultado[28];
        $this -> lugarNacimiento = $resultado[29];
        $this -> nacionalidad = $resultado[30];
        $this -> fechaAfilacionARL = $resultado[31];
        $this -> ARL = $resultado[32];
        $this -> fechaAfilacionEPS = $resultado[33];
        $this -> EPS = $resultado[34];
        $this -> fechaAfilacionCCF = $resultado[35];
        $this -> AFP = $resultado[36];
        $this -> residencia = $resultado[37];
        $this -> barrioResidencia = $resultado[38];
        $this -> direccionResidencia = $resultado[39];
        $this -> tipoPerfil = $resultado[40];
        $this -> tipoTrabajo = $resultado[41];
        $this -> poblacionVulnerable = $resultado[42];        
    }
//--------------------------------------------------------------------------------------------------------------

    public function editarColumn($column,$editableObj,$itemm){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> editarColumn($column,$editableObj,$itemm));
        $this -> conexion -> cerrar();
        return $this -> conexion -> getResultado();
    }

    public function editar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> editar());
        $this -> conexion -> cerrar();
    }

    public function agregar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> agregar2());
        $this -> conexion -> cerrar();
        return $this -> conexion -> getResultado();
    }

    public function eliminar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> eliminar());
        $this -> conexion -> cerrar();
        return $this -> conexion -> getResultado();
    }

//--------------------------------------------------------------------------------------------------------------
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }

     public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarPaginacion($cantidad, $pagina));
        $contrato = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contrato($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12], $resultado[13], $resultado[14], $resultado[15], $resultado[16], $resultado[17], $resultado[18], $resultado[19], $resultado[20], $resultado[21], $resultado[22], $resultado[23], $resultado[24], $resultado[25], $resultado[26], $resultado[27], $resultado[28], $resultado[29], $resultado[30], $resultado[31], $resultado[32], $resultado[33], $resultado[34], $resultado[35], $resultado[36], $resultado[37], $resultado[38], $resultado[39], $resultado[40], $resultado[41], $resultado[42]);
            array_push($contrato, $c);
        }
        $this -> conexion -> cerrar();
        return $contrato;
    }
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarTodos());
        $contrato = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contrato($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12], $resultado[13], $resultado[14], $resultado[15], $resultado[16], $resultado[17], $resultado[18], $resultado[19], $resultado[20], $resultado[21], $resultado[22], $resultado[23], $resultado[24], $resultado[25], $resultado[26], $resultado[27], $resultado[28], $resultado[29], $resultado[30], $resultado[31], $resultado[32], $resultado[33], $resultado[34], $resultado[35], $resultado[36], $resultado[37], $resultado[38], $resultado[39], $resultado[40], $resultado[41], $resultado[42] );
            array_push($contrato, $c);
        }
        $this -> conexion -> cerrar();
        return $contrato;
    }
//--------------------------------------------------------------------------------------------------------------

    public function consultarCantidadFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarCantidadFiltro($filtro));
        $this -> conexion -> cerrar();
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont = 0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }
        return $cont;
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarPaginacionFiltro($cantidad, $pagina, $filtro));
        $contrato = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contrato($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12], $resultado[13], $resultado[14], $resultado[15], $resultado[16], $resultado[17], $resultado[18], $resultado[19], $resultado[20], $resultado[21], $resultado[22], $resultado[23], $resultado[24], $resultado[25], $resultado[26], $resultado[27], $resultado[28], $resultado[29], $resultado[30], $resultado[31], $resultado[32], $resultado[33], $resultado[34], $resultado[35], $resultado[36], $resultado[37], $resultado[38], $resultado[39], $resultado[40], $resultado[41], $resultado[42] );
            array_push($contrato, $c);
        }
        $this -> conexion -> cerrar();
        return $contrato;
    }
//--------------------------------------------------------------------------------------------------------------
    public function consultarCantidadAtributo($filtro, $atributo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarCantidadAtributo($filtro, $atributo));
        $this -> conexion -> cerrar();
        if(($this -> conexion -> extraer()) != null){
            $cont = $this -> conexion -> extraer()[0];
        }else{
            $cont = 0; // si no hay registro  manda cero, para evitar errores por valor nulo
        }
        return $cont;
    }

    public function consultarPaginacionAtributo($cantidad, $pagina, $filtro, $atributo){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> contratoDAO -> consultarPaginacionAtributo($cantidad, $pagina, $filtro, $atributo) );
        $contrato = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Contrato($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7], $resultado[8], $resultado[9], $resultado[10], $resultado[11], $resultado[12], $resultado[13], $resultado[14], $resultado[15], $resultado[16], $resultado[17], $resultado[18], $resultado[19], $resultado[20], $resultado[21], $resultado[22], $resultado[23], $resultado[24], $resultado[25], $resultado[26], $resultado[27], $resultado[28], $resultado[29], $resultado[30], $resultado[31], $resultado[32], $resultado[33], $resultado[34], $resultado[35], $resultado[36], $resultado[37], $resultado[38], $resultado[39], $resultado[40], $resultado[41], $resultado[42]);
            array_push($contrato, $c);
        }
        $this -> conexion -> cerrar();
        return $contrato;
    }

//--------------------------------------------------------------------------------------------------------------
public function datos(){
    $datos = $this ->item."||".    
    $this -> nombre."||".
    $this -> tipoDocumento."||".
    $this -> numDocumento."||".    
    $this -> cargo."||".
    $this -> telefono1."||".
    $this -> telefono2."||".
    $this -> correo."||".
    $this -> salario."||".
    $this -> tipoContrato."||".
    $this -> estadoContrato."||".
    $this -> proyecto."||".
    $this -> fechaInicioProceso."||".
    $this -> fechaEnvioConsorcio."||".
    $this -> fechaPreAprobado."||".
    $this -> fechaEnvioInterventoria."||".
    $this -> fechaRespuestaInterventoria."||".
    $this -> fechaIngresoALaborar."||".
    $this -> fechaTerminacionContrato."||".
    $this -> tipoTerminacionContrato."||".
    $this -> fechaEntregaCarnet."||".
    $this -> fechaEntregaDotacion."||".
    $this -> entidadBancaria."||".
    $this -> certificacionBancaria."||".
    $this -> sexo."||".
    $this -> edad."||".
    $this -> fechadeExpedicion."||".
    $this -> lugarExpedicion."||".
    $this -> fechaNacimiento."||".
    $this -> lugarNacimiento."||".
    $this -> nacionalidad."||".
    $this -> fechaAfilacionARL."||".
    $this -> ARL."||".
    $this -> fechaAfilacionEPS."||".
    $this -> EPS."||".
    $this -> fechaAfilacionCCF."||".
    $this -> AFP."||".
    $this -> residencia."||".
    $this -> barrioResidencia."||".
    $this -> direccionResidencia."||".
    $this -> tipoPerfil."||".
    $this -> tipoTrabajo."||".
    $this -> poblacionVulnerable;

    return $datos;
    }

}

?>