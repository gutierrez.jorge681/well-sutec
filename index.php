<?php
session_start();
require_once "logica/Administrador.php";

$pid = "";
if (isset($_GET["pid"])) {
	$pid = base64_decode($_GET["pid"]);
} else {
	$_SESSION["id"] = "";
	$_SESSION["rol"] = "";
}
if (isset($_GET["cerrarSesion"]) || !isset($_SESSION["id"])) {
	$_SESSION["id"] = "";
}
?>

<html>
<head>
    <title>Sutec Gestion Matriz</title>
    <link rel="icon" type="image/png" href="img/logo.png" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.js"></script>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body clas="mt-2 mb-5">
    <?php 
	$paginasSinSesion = array(
		"presentacion/autenticar.php",
		"presentacion/login.php",
		"presentacion/administrador/sesion.php",
		"presentacion/usuarios/usuarios.php"
	);

	if ($_SESSION["id"] != "") {
		if ($_SESSION["rol"] == "ADMINISTRADOR") {
			include "presentacion/administrador/menuAdministrador.php";
		} else if ($_SESSION["rol"] == "CONSULTOR") {
			include "presentacion/consultor/menuConsultor.php";
		} else if ($_SESSION["rol"] == "EDITOR") {
			include "presentacion/editor/menuEditor.php";
		}
		include $pid;

	}else if (in_array($pid, $paginasSinSesion)) { 

		include $pid;
	}else{
		include "presentacion/login.php";
	} 		

    ?>    

</body>

</html>