<?php

class ContratoDAO{
    private $item;
    private $nombre;
    private $tipoDocumento;
    private $numDocumento;
    private $cargo;
    private $telefono1;
    private $telefono2;
    private $correo;
    private $salario;
    private $tipoContrato;
    private $estadoContrato;
    private $proyecto;
    private $fechaInicioProceso;
    private $fechaEnvioConsorcio;
    
    private $fechaPreAprobado;

    private $fechaEnvioInterventoria;
    private $fechaRespuestaInterventoria;
    private $fechaIngresoALaborar;
    private $fechaTerminacionContrato;
    private $tipoTerminacionContrato;
    private $fechaEntregaCarnet;
    private $fechaEntregaDotacion;
    private $entidadBancaria;
    private $certificacionBancaria;
    private $sexo;
    private $edad;
    
    private $fechadeExpedicion;
    private $lugarExpedicion;
    
    private $fechaNacimiento;
    private $lugarNacimiento;
    
    private $nacionalidad;
    private $fechaAfilacionARL;
    private $ARL;
    private $fechaAfilacionEPS;
    private $EPS;
    private $fechaAfilacionCCF;
    
    private $AFP;

    private $residencia;
    private $barrioResidencia;
    private $direccionResidencia;
    private $tipoPerfil;
    private $tipoTrabajo;
    private $poblacionVulnerable;

    public function  __construct( $item ="", $nombre ="", $tipoDocumento ="", $numDocumento ="", $cargo ="", $telefono1 ="", $telefono2 ="", $correo ="", $salario ="", $tipoContrato ="", $estadoContrato ="", $proyecto ="", $fechaInicioProceso ="", $fechaEnvioConsorcio ="", $fechaPreAprobado ="", $fechaEnvioInterventoria ="", $fechaRespuestaInterventoria ="", $fechaIngresoALaborar ="", $fechaTerminacionContrato ="", $tipoTerminacionContrato ="", $fechaEntregaCarnet ="", $fechaEntregaDotacion ="", $entidadBancaria ="", $certificacionBancaria ="", $sexo ="", $edad ="", $fechadeExpedicion ="", $lugarExpedicion ="", $fechaNacimiento ="", $lugarNacimiento ="", $nacionalidad ="", $fechaAfilacionARL ="", $ARL ="", $fechaAfilacionEPS ="", $EPS ="", $fechaAfilacionCCF ="", $AFP ="", $residencia ="", $barrioResidencia ="", $direccionResidencia ="", $tipoPerfil ="", $tipoTrabajo ="", $poblacionVulnerable ="" ){
        $this -> item = $item;
        $this -> nombre = $nombre;
        $this -> tipoDocumento = $tipoDocumento;
        $this -> numDocumento = $numDocumento;
        $this -> cargo = $cargo;
        $this -> telefono1 = $telefono1;
        $this -> telefono2 = $telefono2;
        $this -> correo = $correo;
        $this -> salario = $salario;
        $this -> tipoContrato = $tipoContrato;
        $this -> estadoContrato = $estadoContrato;
        $this -> proyecto = $proyecto;
        $this -> fechaInicioProceso = $fechaInicioProceso;
        $this -> fechaEnvioConsorcio = $fechaEnvioConsorcio;
        $this -> fechaPreAprobado = $fechaPreAprobado;
        $this -> fechaEnvioInterventoria = $fechaEnvioInterventoria;
        $this -> fechaRespuestaInterventoria = $fechaRespuestaInterventoria;
        $this -> fechaIngresoALaborar = $fechaIngresoALaborar;
        $this -> fechaTerminacionContrato = $fechaTerminacionContrato ;
        $this -> tipoTerminacionContrato = $tipoTerminacionContrato ;
        $this -> fechaEntregaCarnet = $fechaEntregaCarnet;
        $this -> fechaEntregaDotacion = $fechaEntregaDotacion ;
        $this -> entidadBancaria = $entidadBancaria;
        $this -> certificacionBancaria = $certificacionBancaria ;
        $this -> sexo = $sexo;
        $this -> edad = $edad;
        $this -> fechadeExpedicion = $fechadeExpedicion;
        $this -> lugarExpedicion = $lugarExpedicion;
        $this -> fechaNacimiento = $fechaNacimiento;
        $this -> lugarNacimiento = $lugarNacimiento;            
        $this -> nacionalidad = $nacionalidad;        
        $this -> fechaAfilacionARL = $fechaAfilacionARL;
        $this -> ARL = $ARL;
        $this -> fechaAfilacionEPS = $fechaAfilacionEPS;
        $this -> EPS = $EPS;
        $this -> fechaAfilacionCCF = $fechaAfilacionCCF;
        $this -> AFP = $AFP;
        $this -> residencia = $residencia;
        $this -> barrioResidencia = $barrioResidencia;
        $this -> direccionResidencia = $direccionResidencia;
        $this -> tipoPerfil = $tipoPerfil;
        $this -> tipoTrabajo = $tipoTrabajo;
        $this -> poblacionVulnerable = $poblacionVulnerable;
    }
//--------------------------------------------------------------------------------------------------------------

    public function consultar(){
        return "select  'item', 'nombre', 'tipoDocumento', 'numDocumento', 'cargo', 'telefono1', 'telefono2', 'correo', 'salario', 'tipoContrato', 'estadoContrato', 'proyecto', 'fechaInicioProceso', 'fechaEnvioConsorcio', 'fechaPreAprobado', 'fechaEnvioInterventoria', 'fechaRespuestaInterventoria', 'fechaIngresoALaborar', 'fechaTerminacionContrato', 'tipoTerminacionContrato', 'fechaEntregaCarnet', 'fechaEntregaDotacion', 'entidadBancaria', 'certificacionBancaria', 'sexo', 'edad', 'fechadeExpedicion', 'lugarExpedicion', 'fechaNacimiento', 'lugarNacimiento', 'nacionalidad', 'fechaAfilacionARL', 'ARL', 'fechaAfilacionEPS', 'EPS', 'fechaAfilacionCCF', 'AFP', 'residencia', 'barrioResidencia', 'direccionResidencia', 'tipoPerfil', 'tipoTrabajo', 'poblacionVulnerable'
                from contrato
                where item = '" . $this -> item .  "'";
    } 

//--------------------------------------------------------------------------------------------------------------

    public function editarColumn($column,$editableObj,$itemm){
        $sql = "update contrato set " . $column . " = '".$editableObj."' where  item=".$itemm;
        return $sql;
    }

    public function editar(){
        $sql = "update contrato set nombre ='".$nombre."', tipoDocumento ='".$tipoDocumento."', numDocumento ='".$numDocumento."', cargo ='".$cargo."', telefono1 ='".$telefono1."', telefono2 ='".$telefono2."', correo ='".$correo."', salario ='".$salario."', tipoContrato ='".$tipoContrato."', estadoContrato ='".$estadoContrato."', proyecto ='".$proyecto."', fechaInicioProceso ='".$fechaInicioProceso."', fechaEnvioConsorcio ='".$fechaEnvioConsorcio."', fechaPreAprobado ='".$fechaPreAprobado."', fechaEnvioInterventoria ='".$fechaEnvioInterventoria."', fechaRespuestaInterventoria ='".$fechaRespuestaInterventoria."', fechaIngresoALaborar ='".$fechaIngresoALaborar."', fechaTerminacionContrato ='".$fechaTerminacionContrato."', tipoTerminacionContrato ='".$tipoTerminacionContrato."', fechaEntregaCarnet ='".$fechaEntregaCarnet."', fechaEntregaDotacion ='".$fechaEntregaDotacion."', entidadBancaria ='".$entidadBancaria."', certificacionBancaria ='".$certificacionBancaria."', sexo ='".$sexo."', edad ='".$edad."', fechadeExpedicion ='".$fechadeExpedicion."', lugarExpedicion ='".$lugarExpedicion."', fechaNacimiento ='".$fechaNacimiento."', lugarNacimiento ='".$lugarNacimiento."', nacionalidad ='".$nacionalidad."', fechaAfilacionARL ='".$fechaAfilacionARL."', ARL ='".$ARL."', fechaAfilacionEPS ='".$t."', EPS ='".$t."', fechaAfilacionCCF ='".$fechaAfilacionCCF."', AFP ='".$AFP."', residencia ='".$residencia."', barrioResidencia ='".$barrioResidencia."', direccionResidencia ='".$direccionResidencia."', tipoPerfil ='".$tipoPerfil."', tipoTrabajo ='".$tipoTrabajo."', poblacionVulnerable ='".$poblacionVulnerable."' where  item=".$item;
        return $sql;
    }

    public function agregar(){
        return "insert into contrato ( 'item', 'nombre', 'tipoDocumento', 'numDocumento', 'cargo', 'telefono1', 'telefono2', 'correo', 'salario', 'tipoContrato', 'estadoContrato', 'proyecto', 'fechaInicioProceso', 'fechaEnvioConsorcio', 'fechaPreAprobado', 'fechaEnvioInterventoria', 'fechaRespuestaInterventoria', 'fechaIngresoALaborar', 'fechaTerminacionContrato', 'tipoTerminacionContrato', 'fechaEntregaCarnet', 'fechaEntregaDotacion', 'entidadBancaria', 'certificacionBancaria', 'sexo', 'edad', 'fechadeExpedicion', 'lugarExpedicion', 'fechaNacimiento', 'lugarNacimiento', 'nacionalidad', 'fechaAfilacionARL', 'ARL', 'fechaAfilacionEPS', 'EPS', 'fechaAfilacionCCF', 'AFP', 'residencia', 'barrioResidencia', 'direccionResidencia', 'tipoPerfil', 'tipoTrabajo', 'poblacionVulnerable' )
                values ('" . $this -> item . "', '" . $this -> nombre . "', '" . $this -> tipoDocumento . "', '" . $this -> numDocumento .
                 "', '" . $this -> cargo . "', '" . $this -> telefono1 . "', '" . $this -> telefono2 . "', '" . $this -> correo . 
                 "', '" . $this -> salario . "', '" . $this -> tipoContrato . "', '" . $this -> estadoContrato . "', '" . $this -> proyecto . 
                 "', '" . $this -> fechaInicioProceso . "', '" . $this -> fechaEnvioConsorcio ."', '" . $this -> fechaPreAprobado . 
                 "', '" . $this -> fechaEnvioInterventoria . "', '" . $this -> fechaRespuestaInterventoria . "', '" . $this -> fechaIngresoALaborar . 
                 "', '" . $this -> fechaTerminacionContrato . "', '" . $this -> tipoTerminacionContrato . "', '" . $this -> fechaEntregaCarnet . 
                 "', '" . $this -> fechaEntregaDotacion . "', '" . $this -> entidadBancaria . "', '" . $this -> certificacionBancaria . 
                 "', '" . $this -> sexo . "', '" . $this -> edad . "', '" . $this -> fechadeExpedicion . "', '" . $this -> lugarExpedicion . 
                 "', '" . $this -> fechaNacimiento . "', '" . $this -> lugarNacimiento . "', '" . $this -> nacionalidad . 
                 "', '" . $this -> fechaAfilacionARL . "', '" . $this -> ARL . "', '" . $this -> fechaAfilacionEPS . 
                 "', '" . $this -> EPS . "', '" . $this -> fechaAfilacionCCF . "', '" . $this -> AFP . 
                 "', '" . $this -> residencia . "', '" . $this -> barrioResidencia . "', '" . $this -> direccionResidencia . "', '" . $this -> tipoPerfil . "', '" . $this -> tipoTrabajo . "', '" . $this -> poblacionVulnerable . "');";
    } 
    public function agregar2(){
        return "INSERT INTO contrato (item, nombre, tipoDocumento, numDocumento, cargo, telefono1, telefono2, `correo`, `salario`, `tipoContrato`, `estadoContrato`, `proyecto`, `fechaInicioProceso`, `fechaEnvioConsorcio`, `fechaPreAprobado`, `fechaEnvioInterventoria`, `fechaRespuestaInterventoria`, `fechaIngresoALaborar`, `fechaTerminacionContrato`, `tipoTerminacionContrato`, `fechaEntregaCarnet`, `fechaEntregaDotacion`, `entidadBancaria`, `certificacionBancaria`, `sexo`, `edad`, `fechadeExpedicion`, `lugarExpedicion`, `fechaNacimiento`, `lugarNacimiento`, `nacionalidad`, `fechaAfilacionARL`, `ARL`, `fechaAfilacionEPS`, `EPS`, `fechaAfilacionCCF`, `AFP`, `residencia`, `barrioResidencia`, `direccionResidencia`, `tipoPerfil`, `tipoTrabajo`, `poblacionVulnerable`) 
        VALUES (" . $this -> item . ",'" . $this -> nombre . "', '" . $this -> tipoDocumento . "', '" . $this -> numDocumento . 
        "', '" . $this -> cargo . "', '" . $this -> telefono1 . "', '" . $this -> telefono2 . "', '" . $this -> correo . "',  '" . $this -> salario . 
        "', '" . $this -> tipoContrato . "', '" . $this -> estadoContrato . "', '" . $this -> proyecto . "', '" . $this -> fechaInicioProceso . 
        "', '" . $this -> fechaEnvioConsorcio . "', '" . $this -> fechaPreAprobado . "', '" . $this -> fechaEnvioInterventoria . "', '" . $this -> fechaRespuestaInterventoria . "', '" . $this -> fechaIngresoALaborar . 
        "', '" . $this -> fechaTerminacionContrato . "', '" . $this -> tipoTerminacionContrato . "', '" . $this -> fechaEntregaCarnet . "', '" . $this -> fechaEntregaDotacion . "', '" . $this -> entidadBancaria . "', '" . $this -> certificacionBancaria . 
        "', '" . $this -> sexo . "', '" . $this -> edad . "', '" . $this -> fechadeExpedicion . "', '" . $this -> lugarExpedicion . "', '" . $this -> fechaNacimiento .
        "', '" . $this -> lugarNacimiento . "', '" . $this -> nacionalidad ."', '" . $this -> fechaAfilacionARL . "', '" . $this -> ARL . 
        "', '" . $this -> fechaAfilacionEPS . "', '" . $this -> EPS . "', '" . $this -> fechaAfilacionCCF . "', '" . $this -> AFP . 
        "', '" . $this -> residencia . "', '" . $this -> barrioResidencia . "', '" . $this -> direccionResidencia . "', '" . $this -> tipoPerfil . "', '" . $this -> tipoTrabajo . "', '" . $this -> poblacionVulnerable . "')";
    } 
    public function eliminar(){
        return "delete from  contrato WHERE item = '".$this -> item ."' ";
    } 

//--------------------------------------------------------------------------------------------------------------

    public function consultarCantidad(){
        return "select count(item)
                from contrato";
    }
//--------------------------------------------------------------------------------------------------------------

    public function consultarPaginacion($cantidad, $pagina){
        return "select *
                from contrato
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    } 
    
    public function consultarTodos(){
        return "select *
                from contrato";
    } 
//--------------------------------------------------------------------------------------------------------------
    
    public function consultarCantidadFiltro($filtro){
        return "select count(item)
                from contrato
                where item like '%" . $filtro . "%' or numDocumento like '" . $filtro . "%' or nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%' or nacionalidad like '" . $filtro . "%' or estadoContrato like '" . $filtro . "%' or cargo like '" . $filtro . "%' or fechaIngresoALaborar like '" . $filtro . "%' or fechaTerminacionContrato like '" . $filtro . "%' or poblacionVulnerable like '" . $filtro . "%' or telefono1 like '" . $filtro . "%' or telefono2 like '" . $filtro . "%' or edad like '" . $filtro . "%'   ";             
    }

    public function consultarPaginacionFiltro($cantidad, $pagina, $filtro){
        return "select *
                from contrato
                where item like '%" . $filtro . "%' or numDocumento like '" . $filtro . "%' or nombre like '" . $filtro . "%' or apellido like '" . $filtro . "%' or nacionalidad like '" . $filtro . "%' or estadoContrato like '" . $filtro . "%' or cargo like '" . $filtro . "%' or fechaIngresoALaborar like '" . $filtro . "%' or fechaTerminacionContrato like '" . $filtro . "%' or poblacionVulnerable like '" . $filtro . "%' or telefono1 like '" . $filtro . "%' or telefono2 like '" . $filtro . "%' or edad like '" . $filtro . "%' 
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

//--------------------------------------------------------------------------------------------------------------
    public function consultarCantidadAtributo($filtro, $atributo){
        return "select count(item)
                from contrato
                where ". $atributo ." like '%" . $filtro . "'";
    }

    public function consultarPaginacionAtributo($cantidad, $pagina, $filtro, $atributo ){
        return "select *
                from contrato
                where ". $atributo ." like '%" . $filtro . "'
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

//--------------------------------------------------------------------------------------------------------------    
}

?>